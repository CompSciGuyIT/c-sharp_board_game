﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Player_Class_Library;
using Board_Class_Library;

namespace Square_Class_Library
{
    public class Chance_Square : Square
    {
        public Chance_Square(string name, int number)
            : base(name, number)
        {
            
        }

        public override void EffectOnPlayer(Player who)
        {
            Console.WriteLine();
            Console.WriteLine("current square No. " + who.Location.GetNumber());

            Random random = new Random();
            int randomNumber = random.Next(10);
            if (randomNumber % 2 == 0)
            {
                who.Deduct(50);

                Console.WriteLine("money: $" + who.Money);

                int new_position = who.Location.GetNumber() - 5;

                who.Location = Board.GetGameBoardSquare(new_position);
                who.Location.EffectOnPlayer(who);

                Console.WriteLine("next square No. " + who.Location.GetNumber());
                Console.WriteLine("-----------------------------------------------------------");

            }
            else
            {
                who.Add(50);
                int new_position = who.Location.GetNumber() + 5;

                // Makes sure the new position never exceeds the finishing square
                if (new_position > Board.FINISH)
                {
                    new_position = Board.FINISH;
                }

                who.Location = Board.GetGameBoardSquare(new_position);
                who.Location.EffectOnPlayer(who);
            }
        }
    }
}
