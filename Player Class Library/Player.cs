﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Square_Class_Library;
using Die_Class_Library;

namespace Player_Class_Library {

    public class Player 
    {
        private string name;
        private int money;
        private bool hasWon;
        private Square location;
        private Image playerTokenImage;
        private Brush playerTokenColour;
        private Die die1, die2;

        public Player()
        {
            throw new ArgumentException("Do not use this constructor!");
        }

        public Player(string name, Square location)
        {
            this.name = name;
            this.location = location;
            this.money = 100;
        }

        public void Add(int amount)
        {
            money += amount;
        }

        public void Deduct(int amount)
        {
            money -= amount;
            if (money < 0)
            {
                money = 0;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        } //end Name Property

        public Square Location
        {
            get
            {
                return location;
            }

            set
            {
                location = value;
            }
        } //end Location Property

        public int Money
        {
            get
            {
                return money;
            }

            set
            {
                money = value;
            }
        } //end Money Property

        public bool HasWon
        {
            get
            {
                return hasWon;
            }

            set
            {
                hasWon = value;
            }
        } //end HasWon Property

        public Image PlayerTokenImage
        {
            get
            {
                return playerTokenImage;
            }

        } //end PlayerTokenImage Property

        public Brush PlayerTokenColour
        {
            get
            {
                return playerTokenColour;
            }

            set
            {
                playerTokenColour = value;
                playerTokenImage = new Bitmap(1, 1);
                using(Graphics g = Graphics.FromImage(playerTokenImage))
                {
                    g.FillRectangle(playerTokenColour, 0, 0, 1, 1);
                }
            }
        } //end PlayerTokenColour Property

        public int RollTheDice(Die die1, Die die2)
        {
            this.die1 = die1;
            this.die2 = die2;
            return die1.Roll() + die2.Roll();
        }

        // Part E section 2
        public int Roll_Dice_Again()
        {
            return RollTheDice(die1, die2);
        }
   
    }
}
