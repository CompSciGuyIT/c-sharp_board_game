﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Player_Class_Library;
using Board_Class_Library;
using System.Drawing;
using System.Diagnostics;
using Square_Class_Library;
using Die_Class_Library;

namespace HareAndTortoise {
    public static class HareAndTortoise_Game {

        static public string[] name = { "One", "Two", "Three", "Four", "Five", "Six" };
        static public Brush[] colour = { Brushes.Black, Brushes.Red, Brushes.Gold, Brushes.GreenYellow, Brushes.Fuchsia, Brushes.BlueViolet };
        static private int numberOfPlayers = 6;
        static public int NumberOfPlayers { get { return numberOfPlayers; } set { numberOfPlayers = value; } }
        static public Die die1 = new Die();
        static public Die die2 = new Die();

        static public List<int> most_money = new List<int>();
        static public List<int> winners = new List<int>();
        static public int max_dollars = 0;
        static public int max_square = 0;
        static public bool game_finished = false;

        private static BindingList<Player> players = new BindingList<Player>();
        public static BindingList<Player> Players {
            get {
                return players;
            }
        }

        /// <summary>
        /// Sets up the game board and initialises the players
        /// </summary>
        public static void SetUpGame(){

            Board.SetUpBoard();
 
            //more code to be added later
            InitialisePlayers();

        }// end SetUpGame

        /// <summary>
        /// Creates the player objects and adds them to a binding list
        /// </summary>
        public static void InitialisePlayers()
        {
            players = new BindingList<Player>();
            for (int i = 0; i < NumberOfPlayers; i++)
            {
                Player p = new Player(name[i], Board.StartSquare());
                p.PlayerTokenColour = colour[i];
                players.Add(p);
            }
        }

        /// <summary>
        /// Initiates a single round of the game
        /// </summary>
        /// <returns>Whether a player has landed on the finishing square or not</returns>
        public static bool PlayOneRound()
        {
            for (int player = 0; player < NumberOfPlayers; player++)
            {
                Square current = Players[player].Location;

                int total_rolled = Players[player].RollTheDice(die1, die2);
                int old_position = current.GetNumber();
                int new_position = old_position + total_rolled;

                // Makes sure the new position never exceeds the finishing square
                if (new_position > Board.FINISH)
                {
                    new_position = Board.FINISH;
                }

                Players[player].Location = Board.GetGameBoardSquare(new_position);
                Players[player].Location.EffectOnPlayer(Players[player]);

                HareAndTortoise_Form.UpdateDataGridView();
            }

            game_finished = Check_End_Of_Game();

            if (game_finished)
                Determine_Winner();

            return game_finished;
        }

        /// <summary>
        /// Checks to see if a player has landed on the finishing square
        /// </summary>
        /// <returns>Whether the game has finishied or not</returns>
        private static bool Check_End_Of_Game()
        {
            // Signals the end of the game
            for (int player = 0; player < NumberOfPlayers; player++)
            {
                Square current = Players[player].Location;
                if (current.GetNumber() == Board.FINISH)
                {
                    game_finished = true;
                }
            }

            return game_finished;
        }

        /// <summary>
        /// Determines the winner/s when a player lands on the finishing square
        /// </summary>
        private static void Determine_Winner()
        {
            // Find player/s with the most money
            for (int player = 0; player < NumberOfPlayers; player++)
            {
                if (Players[player].Money >= max_dollars)
                {
                    if (Players[player].Money > max_dollars)
                    {
                        most_money.Clear();
                    }

                    most_money.Add(player);
                    max_dollars = Players[player].Money;
                }
            }

            // If there is more that one player with the highest amount of money
            if (most_money.Count > 1)
            {
                // find which player/s have advanced the most squares
                for (int player = 0; player < most_money.Count; player++)
                {
                    if (Players[most_money[player]].Location.GetNumber() >= max_square)
                    {
                        if (Players[most_money[player]].Location.GetNumber() > max_square)
                        {
                            winners.Clear();
                        }

                        winners.Add(most_money[player]);
                        max_square = Players[most_money[player]].Location.GetNumber();
                    }
                }

                // Declare the winner/s
                for (int player = 0; player < winners.Count; player++)
                {
                    Players[winners[player]].HasWon = true;
                }
            }

            // if there is only one player with the highest amount of money
            // Declare the winner
            else
            {
                Players[most_money[0]].HasWon = true;
            }
        }

        // MORE METHODS TO BE ADDED HERE LATER

        public static void OutputAllPlayerDetails()
        {
            for (int i = 0; i < NumberOfPlayers; i++)
            {
                OutputIndividualDetails(Players[i]);
            }
        } // end OutputAllPlayerDetails


        /// <summary>
        /// Outputs a player's current location and amount of money
        /// pre:  player's object to display
        /// post: displayed the player's location and amount
        /// </summary>
        /// <param name="who">the player to display</param>
        public static void OutputIndividualDetails(Player who)
        {
            Square playerLocation = who.Location;
            Trace.WriteLine(String.Format("Player {0} on square with {1:C}",
                                           who.Name,  who.Money));
        }// end OutputIndividualDetails

        
    }//end class
}//end namespace
